
function uwuify(text) {
    const faceList = [
        //basic faces
        'UwU',
        '>_<',
        '0.o',
        ':3',
        '^^',
        '>w<',
        //fancy faces
        '(///ˬ///✿)',
        'σωσ',
        'ฅ^•ﻌ•^ฅ'
    ]
    const wordList = [
        //words
        'nyaa~~',
        'mya~',
        'rawr xD',
        'rawr x3',
        'rawr',
        '*pownces on youwe*'
    ]
    const randomInsert = (list) => {
        return list[Math.floor(Math.random() * list.length)];
    }
    const randomWeightedBool = (percentage) => {
        return Math.random() < (percentage / 100);
    }


    const words = text.split(' ');

    wordsOut = [];
    let startOfScentance = true;
    words.forEach((word, i) => {
        //analyze word
        
        let endOfScentance = word.includes('.');
        if (i == words.length - 1) {endOfScentance = true;}


        // Replace l,r with w
        word = word.replace(/[lr]/g, 'w');
        word = word.replace(/[LR]/g, 'W');

        if (endOfScentance) {
            if (randomWeightedBool(20)) {
                word = word + ' ' + randomInsert(faceList);
            }
            
            if (randomWeightedBool(20)) {
                word = `${word} ${randomInsert(wordList)}`;
            }
        }
        if (randomWeightedBool(10)) {
            word = `${word} ${randomInsert(faceList)}${endOfScentance ? '.' : ''}`;
        }


        //stutter
        if (randomWeightedBool( startOfScentance ? 75 : 10 )) {
            word = word[0] + '-' + word;
        }

        
        // signal start of sentance
        startOfScentance = false;
        if (endOfScentance) {startOfScentance = true;}

        wordsOut.push(word);
    });
    return wordsOut.join(' ');
}

console.log(uwuify(`hello there, I'm ashley. how are you?`))
